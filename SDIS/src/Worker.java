import java.util.ArrayList;
import java.util.Queue;


public class Worker implements Runnable{
	private int id;
	public Queue<int[]> queue;
	public ArrayList finished;
	private boolean busy;

	public Worker(int id,Queue<int[]> q, ArrayList l) throws InterruptedException{
		this.id=id;
		this.queue=q;
		this.finished=l;
		this.busy = false;
	}


	private int[] getMessage(){
		return this.queue.poll();
	}

	private void postReply(int messId){
		this.finished.add(messId);
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){
			if(!queue.isEmpty()){
				int[] mess = getMessage();
				int messId = mess[0];
				busy=true;
				try {
					Thread.sleep(mess[1]);
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				postReply(messId);
				busy=false;
			}

		}
	}
	
	public void algo(){
		// Pour chaque message posté dans finished stocké son temps de traitement
		// toutes les minutes compter le temps d'attente total tae
		//quand tae supérieur à 30s doubler le nombre de  workers
		// chaquoi fois que 30% des workers sont inactifs pendant 30s diviser le nombre de workers par 1.5
	}

}
