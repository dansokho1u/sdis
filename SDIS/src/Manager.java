import java.util.ArrayList;
import java.util.Queue;

public class Manager {

	private ArrayList workers;
	public ArrayList finished;
	public int workersNumber;
	public Queue<int[]> queue;
	
	public Manager(Queue<int[]> q, ArrayList f){
		workersNumber = 0;
		workers= new ArrayList();
		finished = f;
		
	}
	
	public void addWorker() throws InterruptedException{
		Worker w = new Worker(workersNumber, queue, finished);
		workers.add(w);
		workersNumber++;
		new Thread(w).start();
	}
	
	public void removeWorker(Worker w){
		workers.remove(w);
		workersNumber--;
	}
}
